// Ensure ThreeJS is in global scope for the 'examples/'
global.THREE = require("three");

// Include any additional ThreeJS examples below
require("three/examples/js/controls/OrbitControls");

const canvasSketch = require("canvas-sketch");
const random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');

const settings = {
  // Make the loop animated
  animate: true,
  // Get a WebGL canvas rather than 2D
  context: "webgl",
  attribute: { antialias: true }
};

const sketch = ({ context }) => {
  // Create a renderer
  const renderer = new THREE.WebGLRenderer({
    canvas: context.canvas
  });

  // WebGL background color
  renderer.setClearColor("hsl(0, 0%, 95%)", 1); // (color, transparency)

  // Setup a camera
  // const camera = new THREE.PerspectiveCamera(45, 1, 0.01, 100); // (field of view, aspect ratio, near value, far value)
  const camera = new THREE.OrthographicCamera(); // (field of view, aspect ratio, near value, far value)
  // camera.position.set(4, 2, 2); //(x, y , z)
  // camera.lookAt(new THREE.Vector3()); // look at center of world)

  // Setup camera controller
  // const controls = new THREE.OrbitControls(camera, context.canvas); // mouse setup interaction

  // Setup your scene
  const scene = new THREE.Scene();

  // Setup a geometry
  // const geometry = new THREE.SphereGeometry(1, 32, 16);
  const geometry = new THREE.BoxGeometry(1, 1, 1);

  // Setup a material
  // const material = new THREE.MeshBasicMaterial({
  //   color: "red",
  //   wireframe: true
  // });

  const material = new THREE.MeshBasicMaterial({
    color: 'red',
    // roughness: 0.75,
    // flatShading: true
  });

  // Setup a mesh with geometry + material
  // const mesh = new THREE.Mesh(geometry, material);
  // scene.add(mesh);

  const palette =  random.pick(palettes);

  for (let i = 0; i < 30; i++) {
    // const mesh = new THREE.Mesh(geometry, material);
    const mesh = new THREE.Mesh(geometry, new THREE.MeshStandardMaterial({
      color: random.pick(palette),
      })
    );
    mesh.position.set(random.range(-1, 1), random.range(-1, 1), random.range(-1, 1));
    mesh.scale.set(random.range(-1, 1), random.range(-1, 1), random.range(-1, 1));
    mesh.scale.multiplyScalar(0.5);
    scene.add(mesh);
  }

  // scene.add(new THREE.AmbientLight('#59314f')); // lights?

  // const light = new THREE.PointLight('#45caf7', 1, 15.5); //light?
  // light.position.set(2, 2, -4).multiplyScalar(1.5);
  // scene.add(light);

  // draw each frame

  scene.add(new THREE.AmbientLight('hsl(0, 0%, 20%)'));

  const light = new THREE.DirectionalLight('white', 1);
  light.position.set(0, 0, 4);
  scene.add(light);

  return {
    // Handle resize events here
    resize({ pixelRatio, viewportWidth, viewportHeight }) {
      renderer.setPixelRatio(pixelRatio);
      renderer.setSize(viewportWidth, viewportHeight, false);
      // camera.aspect = viewportWidth / viewportHeight;

      const aspect = viewportWidth / viewportHeight;

      // Ortho zoom
      const zoom = 1.5;
      
      // Bounds
      camera.left = -zoom * aspect;
      camera.right = zoom * aspect;
      camera.top = zoom;
      camera.bottom = -zoom;
      
      // Near/Far
      camera.near = -100;
      camera.far = 100;
      
      // Set position & look at world center
      camera.position.set(zoom, zoom, zoom);
      camera.lookAt(new THREE.Vector3());
      
      // Update the camera
      camera.updateProjectionMatrix();
    },
    // Update & render your scene here
    render({ time }) {
      // mesh.rotation.y = time * (10 * Math.PI / 180); // animation
      // controls.update();
      renderer.render(scene, camera);
    },
    // Dispose of events & renderer for cleaner hot-reloading
    unload() {
      // controls.dispose();
      renderer.dispose();
    }
  };
};

canvasSketch(sketch, settings);
