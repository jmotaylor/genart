const canvasSketch = require('canvas-sketch');
const { lerp } = require('canvas-sketch-util/math');
const random = require('canvas-sketch-util/random');
const palettes = require('nice-color-palettes');

random.setSeed(random.getRandomSeed());

const settings = {
  // dimensions: 'A4',
  // orientation: 'landscape',
  // units: 'cm',
  // pixelsPerInch: 300
  suffix: random.getSeed(),
  dimensions: [2048, 2048]
};


console.log(random.getSeed());

const margin = 400;
const gridCount = 30;
const circleRadius = 5;

// random.setSeed(510);

const sketch = () => {
  // const palette = random.pick(palettes).slice(0,3);
  const colorCount = random.rangeFloor(2, 6);
  const palette = random.shuffle(random.pick(palettes)).slice(0, colorCount); // shuffle palette then remove some
  // const palette = random.pick(palettes);

  const createGrid = () => {
    const points = [];
    //const gridCount = 20;
    for (let x = 0; x < gridCount; x++) {
      for (let y = 0; y < gridCount; y++) {
        const u = gridCount <= 1 ? 0.5 : x / (gridCount - 1);
        const v = gridCount <= 1 ? 0.5 : y / (gridCount - 1);
        const radius = Math.abs(random.noise2D(u, v)) * 0.2; // creates more gradual randomness
        points.push({
          color: random.pick(palette),
          // radius: Math.abs(0.01 + random.gaussian() * 0.01), // creates a more varied randomness
          radius,
          position: [ u, v ],
          rotation: random.noise2D(u, v),
        });
      }
    }
    return points;
  }

  // random.setSeed(513);
  const points = createGrid().filter(() => random.value() > 0.5);

  return ({ context, width, height }) => {
    context.fillStyle = 'white';
    context.fillRect(0, 0, width, height);

    points.forEach(data => {
      const {
        color,
        position,
        radius,
        rotation
      } = data;

      const [ u, v ] = position;

      const x = lerp(margin, width - margin, u);
      const y = lerp(margin, height - margin, v);
      // const x = u * width;
      // const y = v * height;

      // context.beginPath();
      // context.arc(x, y, radius * width, 0, Math.PI * 2, false);
      // context.strokeStyle = 'black';
      // context.lineWidth = 20;
      // context.fillStyle = color;
      // context.fill();
      context.save();
      context.fillStyle = color;
      context.font = `${radius * width}px "Helvetica"`;
      context.translate(x, y);
      context.rotate(rotation);
      context.fillText('=', 0, 0);
      context.restore();
    });
  };
};

canvasSketch(sketch, settings);
